/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This class represents the context of one moment(day) of the pumpkins
 * growth.
 *
 */
public class GrowthContext {
	
	private int current;
	private List<GrowthCondition> conditions;
	
	public GrowthContext(int current, List<GrowthCondition> conditions) {
		this.current = current;
		this.conditions = conditions;
	}
	
	/**
	 * Returns true if the day described by an instance of this
	 * class has at least n-1 predecessors, else it returns false.
	 * 
	 * @param n
	 * @return
	 */
	public boolean hasLastConditions(int n) {
		return n - 1 <= current;
	}
	
	/** 
	 * Returns the growth conditions of the last n days, including
	 * the current Day.
	 * 
	 * @param n the maximum number of conditions to return. 
	 * @return returns a list with at most the last n growth conditions.
	 */
	public Collection<GrowthCondition> getLastConditions(int n) {
		
		List<GrowthCondition> list = new ArrayList<GrowthCondition>();
		int last = current - n + 1;
	
		for (int i = current; i >= 0 && i >= last; i--) {
			list.add(conditions.get(i));
		}
		
		return list;
	}
}
