/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class NormalCondition implements GrowthCondition {

	private int water;
	private int light;
	
	/* 0 <= light + water <= 100 
	 * 0 <= light, 0 <= water */
	public NormalCondition(int light, int water) {
		
		assert (light < 0 || water < 0
				|| light + water != 100);
		
		this.light = light;
		this.water = water;
	}
	
	@Override
	public int getPercentWater() {
		return this.water;
	}
	
	@Override
	public int getPercentLight() {
		return this.light;
	}

}
