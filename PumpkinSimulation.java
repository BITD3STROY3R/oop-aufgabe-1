/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;
import java.util.List;

public class PumpkinSimulation {
	
	private Collection<GrowthConstraint> constraints;
	private List<GrowthCondition> conditions;
	
	public PumpkinSimulation(List<GrowthCondition> conditions, 
		Collection<GrowthConstraint> constraints) {
		
		this.constraints = constraints;
		this.conditions  = conditions;
	}
	
	public Pumpkin run() {
		Pumpkin pumpkin = new Pumpkin();
		GrowthPeriod gp = new GrowthPeriod(conditions);
		
		// iterate over all days
		while (gp.hasNextDay()) {
			GrowthContext context = gp.next();
			
			// apply all constraints 
			for(GrowthConstraint c : constraints) {
				c.apply(pumpkin, context);
			}
			pumpkin.grow();
		}
		
		return pumpkin;
	}
}
