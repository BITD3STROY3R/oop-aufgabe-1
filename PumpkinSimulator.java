/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

public class PumpkinSimulator {
	
	private Collection<GrowthConstraint> constraints;
	
	public PumpkinSimulator() {
		
		constraints = new ArrayList<GrowthConstraint>();
	}
	
	/**
	 * Creates a new pumpkin growth simulation
	 * @param time the duration(days) of the simulation
	 * @param c the list of the growth conditions
	 * @return growth simulation
	 */
	public PumpkinSimulation createSimulation(int time, List<GrowthCondition> c) {
		
		return new PumpkinSimulation(adjustConditions(time, c), constraints);
	}
	
	/**
	 * Creates a copy of the growth condition list, and ensures
	 * that the list contains exactly n conditions. Missing entries 
	 * are completed with the (0%,0%) condition. If the input list
	 * contains more than n elements, only the first n elements are copied
	 * @param n the exact number of conditions the 
	 * @param c the list source list
	 * @return a new list with exactly n elements
	 */
	private List<GrowthCondition> adjustConditions(int n, List<GrowthCondition> c) {
		
		// create a copy of the condition list
		ArrayList<GrowthCondition> c1 = new ArrayList<GrowthCondition>();
		
		// replace missing entries with 0% water, 0% light condition
		for (int i = 0; i < c.size(); i++) {
			GrowthCondition cond = c.get(i);
			c1.add(i, cond == null ? NoCondition.getInstance() : cond);
		}
		
		// complete the list with 0%,0% condition
		for (int i = 0; i < n - c.size(); i++) {
			c.add(NoCondition.getInstance());
		}
		
		return c1;
	}
	
	public void addConstraint(GrowthConstraint gc) {
		constraints.add(gc);
	}
}