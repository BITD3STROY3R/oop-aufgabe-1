/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;

public class Humidity extends GrowthConstraint {

	private int weightReduction;
	private int threshold;
	
	public Humidity(int span, int threshold, int weightReduction) {
		super(span);
		this.weightReduction = weightReduction;
		this.threshold = threshold;
	}
	
	@Override
	protected void doAction(Pumpkin p, Collection<GrowthCondition> c) {
		if (checkThreshold(c)) {
			p.decreaseWeight(weightReduction);
		}
	}
	
	private boolean checkThreshold(Collection<GrowthCondition> c) {
		return threshold <= minHumidity(c);
	}
	
	/**
	 * Returns the minimum percentage of water of the conditions.
	 * @param conditions conditions to search for the minimum of water
	 * @return the minimum percentage of water
	 */
	private int minHumidity(Collection<GrowthCondition> conditions) {
		int minHumidity = 100;
		
		for (GrowthCondition c : conditions) {
			int humidity = c.getPercentWater();
			minHumidity = (minHumidity > humidity) ? humidity : minHumidity;
		}
		
		return minHumidity; 
	}
	
	@Override
	public String toString() {
		return super.toString() + "<-Humidity[.threshold = " + threshold
				+ ", strength = " + weightReduction + "]";
	}

}
