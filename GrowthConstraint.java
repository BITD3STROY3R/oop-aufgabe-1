/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;

/**
 * This is the base class for constraints applied to a pumpkin
 * in its growth period. Constraints operate on 
 * a span of time, given in Days. The span determines how many
 * days of the past growth are needed to determine the behavior of
 * the constraint.
 */
public abstract class GrowthConstraint {
	
	private int span;
	
	/**
	 * Constructor
	 * @param span the number of Days the constraint operates on (span > 0)
	 */
	public GrowthConstraint(int span) {
		this.span = span;
	}
	
	/**
	 * Executes the constraint only if at least span days elapsed since the
	 * beginning of the growth.
	 * @param p the pumpkin
	 * @param gp the growth period context
	 */
	public void apply(Pumpkin p, GrowthContext gc) {
		
		if (gc.hasLastConditions(span)) {
			doAction(p, gc.getLastConditions(span));
		}
	}
	
	@Override
	public String toString() {
		return "Constraint[.span = " + span + "]";
	}
	
	/**
	 * Contains the actual logic of the constraint
	 * 
	 * @param p the Pumpkin the constraint is working on 
	 * @param c a collection of the last span conditions
	 */
	protected abstract void doAction(Pumpkin p, Collection<GrowthCondition> c);
	
}
