/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;

public class Climat extends GrowthConstraint {
	
	private int threshold;

	public Climat(int span, int threshold) {
		super(span);
		this.threshold = threshold;
	}
	
	@Override
	protected void doAction(Pumpkin p, Collection<GrowthCondition> c) {
		if (checkThreshold(c)) {
			p.die();
		}
	}
	
	private boolean checkThreshold(Collection<GrowthCondition> c) {
		return countBrightDays(c) < threshold
				|| countWetDays(c) < threshold;
	}
	
	/**
	 * @param conditions The list of conditions to search for bright days
	 * @return the number of days that have an illumination of 100%
	 */
	private int countBrightDays(Collection<GrowthCondition> conditions) {
		int result = 0;
		
		for (GrowthCondition c : conditions) {
			if (c.getPercentLight() == 100) {
				result++;
			}
		}
		return result;
	}
	
	/**
	 * @param conditions The list of conditions to search for wet days
	 * @return the number of days that have a humidity of 100%
	 */
	private int countWetDays(Collection<GrowthCondition> conditions) {
		int result = 0;
		
		for (GrowthCondition c : conditions) {
			if (c.getPercentWater() == 100) {
				result++;
			}
		}
		return result;
	}
}
