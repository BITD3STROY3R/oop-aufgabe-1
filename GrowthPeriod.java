/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.List;

/**
 * This class is used to describe a growth period. Each condition
 * corresponds to one day of the growth.
 */
public class GrowthPeriod {
	
	private int current = 0;
	private List<GrowthCondition> conditions;
	
	public GrowthPeriod(List<GrowthCondition> conditions) {
		
		this.conditions = conditions;
	}
	
	/**
	 * Returns true if the growth period isn't completed,
	 * else it returns false.
	 */
	public boolean hasNextDay() {
		return current < conditions.size();
	}
	
	/**
	 * Sets the growth period context to the next day
	 */
	public GrowthContext next() {
		return new GrowthContext(current++, conditions);
	}
}
