/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 1 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.List;
import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		testAll();
	}
	
	public static void testAll() {
		
		// Setting up the Simulator
		PumpkinSimulator ps = new PumpkinSimulator();
		ps.addConstraint(new Drought(5,10,50));
		ps.addConstraint(new Drought(10,10,100));
		ps.addConstraint(new Humidity(1,50,1));
		ps.addConstraint(new Humidity(2,30,1));
		ps.addConstraint(new Humidity(4,10,1));
		ps.addConstraint(new Illumination());
		ps.addConstraint(new Climat(100, 10));
		
		test1(ps);
		test2(ps);
		test3(ps);
		test4(ps);
		test5(ps);
		test6(ps);
	}
	
	public static void test1(PumpkinSimulator ps) {
		
		List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
		double expected = 1.0;
		
		// Berechne den erwarteten Wert des Gewichts
		for (int i = 0; i < 10; i++) {
			expected -= 0.01 * expected;
			expected += 0.05 * expected;
			
			expected += 0.05 * expected;
			expected += 0.05 * expected;
			
			expected += 0.05 * 0.89 * expected;
			expected += 0.05 * expected;
			
			expected += 0.05 * expected;
			expected += 0.05 * expected;
			
			expected += 0.05 * expected;
			expected += 0.05 * 0.89 * expected;
		}
		
		for (int i = 0; i < 10; i++) {
			conditions.add(new NormalCondition(0, 100));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(100, 0));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(89, 11));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(100, 0));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(100, 0));
			conditions.add(new NormalCondition(89, 11));
		}
		
		Pumpkin p = ps.createSimulation(100, conditions).run();
		
		System.out.println();
		System.out.println("[TEST 1]");
		System.out.println("Wetterbedingungen (% licht, % wasser): [(0, 100) (100,0) (100,0) (100,0) (89, 11) (100,0) (100, 0) (100, 0) (100, 0) (89, 11)] x 10");
		System.out.println("\t60 Tage mit 5% Wachstum");
		System.out.println("\t10 Tage ohne Wachstum");
		System.out.println("\t20 Tage mit schw\u00E4cherem wachstum 0.89 * 5%");
		System.out.println("\t10 x 1-Tages Schneckenangriff, -1% Gewicht");
		System.out.println("Expected weight = " + expected);
		System.out.println(p);	
	}
	
public static void test2(PumpkinSimulator ps) {
		
		List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
		double expected = 1.0;
		
		// Berechne den erwarteten Wert des Gewichts
		for (int i = 0; i < 10; i++) {
			expected -= 0.01 * expected;
			expected += 0.05 * expected;
			
			expected += 0.05 * expected;
			expected += 0.05 * expected;
			
			expected -= 0.01 * expected;
			expected += 0.05 * 0.5 * expected;
			expected -= 0.01 * expected;
			expected += 0.05 * 0.7 * expected;
			
			expected += 0.05 * 0.9 * expected;
			expected -= 0.01 * expected;
			expected += 0.05 * 0.9 * expected;
			
			expected += 0.05 * expected;
			expected += 0.05 * 0.9 * expected;
		}
		
		for (int i = 0; i < 10; i++) {
			conditions.add(new NormalCondition(0, 100));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(100, 0));
			conditions.add(new NormalCondition(100, 0));
			
			conditions.add(new NormalCondition(50, 50));
			conditions.add(new NormalCondition(70, 30));
			
			conditions.add(new NormalCondition(90, 10));
			conditions.add(new NormalCondition(90, 10));
			
			conditions.add(new NormalCondition(100, 0));
			conditions.add(new NormalCondition(90, 10));
		}
		
		Pumpkin p = ps.createSimulation(100, conditions).run();
		
		System.out.println();
		System.out.println("[TEST 2]");
		System.out.println("Wetterbedingungen (% licht, % wasser): [(0, 100) (100,0) (100,0) (100,0) (50, 50) (70,30) (90, 10) (90, 10) (100, 0) (90, 10)] x 10");
		System.out.println("\t50 Tage mit 5%, 20 Tage mit 0.9 * 5%, 10 Tage mit 0.25%, 10 Tage mit 0.7 * 5% Wachstum");
		System.out.println("\t10 Tage ohne Wachstum");
		System.out.println("\t10 x 4-Tages Schneckenangriff(min 10% Feuchtigkeit), -1% Gewicht");
		System.out.println("\t10 x 2-Tages Schneckenangriff(min 30% Feuchtigkeit), -1% Gewicht");
		System.out.println("\t10 x 1-Tages Schneckenangriff(min 50% Feuchtigkeit), -1% Gewicht");
		System.out.println("Expected weight = " + expected);
		System.out.println(p);
	}

	public static void test3(PumpkinSimulator ps) {
	
	List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
	double expected = 1.0;
	
	// Berechne den erwarteten Wert des Gewichts
	for (int i = 0; i < 10; i++) {
		expected += 0.05 * expected;
		expected += 0.05 * expected;
		
		expected += 0.05 * expected;
		expected += 0.05 * expected;
		
		
		expected += 0.05 * 0.5 * expected;
		expected += 0.05 * 0.8 * expected;
		
		expected += 0.05 * expected;
		expected += 0.05 * expected;
		
		expected += 0.05 * expected;
		expected -= 0.01 * expected;
	}
	
	for (int i = 0; i < 10; i++) {
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(80, 20));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(0, 100));
	}
	
	Pumpkin p = ps.createSimulation(100, conditions).run();
	
	System.out.println();
	System.out.println("[TEST 3]");
	System.out.println("Wetterbedingungen (% licht, % wasser): [5x(100,0) (80, 20) 3x(100,0) (0,100)] x 10");
	System.out.println("\t50 Tage mit 5%, 10 Tage mit 0.8 * 5% Wachstum");
	System.out.println("\t10 Tage ohne Wachstum");
	System.out.println("\t10 x 5-Tages Trockenheit(Feuchtigkeit <= 10%), wachsum / 2");
	System.out.println("\t10 x 1-Tages Schneckenangriff(min 50% Feuchtigkeit), -1% Gewicht");
	System.out.println("Expected weight = " + expected);
	System.out.println(p);
}

public static void test4(PumpkinSimulator ps) {
	
	List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
	double expected = 1.0;
	
	expected += 0.05 * expected;
	expected += 0.05 * expected;
	expected += 0.05 * expected;
	expected += 0.05 * expected;
	expected += 0.05 * 0.5 * expected;
	expected += 0.05 * 0.5 * expected;
	expected += 0.05 * 0.5 * expected;
	expected += 0.05 * 0.5 * expected;
	expected += 0.05 * 0.5 * expected;
	expected += 0.05 * 0 * expected;
	
	// Berechne den erwarteten Wert des Gewichts
	for (int i = 0; i < 9; i++) {
		expected -= 0.01 * expected;
		
		expected -= 0.01 * expected;
		expected -= 0.01 * expected;
		
		expected += 0.05 * expected;
		expected += 0.05 * expected;
		
		
		expected += 0.05 * expected;
		expected += 0.05 * 0.8 * expected;
		
		expected += 0.05 * expected;
		expected += 0.05 * expected;
		
		expected += 0.05 * expected;
		expected += 0.05 * expected;
	}
	
	for (int i = 0; i < 10; i++) {
		conditions.add(new NormalCondition(100, 0));
	}
	for (int i = 0; i < 9; i++) {
		conditions.add(new NormalCondition(0, 100));
		conditions.add(new NormalCondition(0, 100));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(80, 20));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
		
		conditions.add(new NormalCondition(100, 0));
		conditions.add(new NormalCondition(100, 0));
	}
	
	Pumpkin p = ps.createSimulation(100, conditions).run();
	
	System.out.println();
	System.out.println("[TEST 4]");
	System.out.println("Wetterbedingungen (% licht, % wasser): 10x(100, 0) [2x(0,0) 3x(100,0) (80, 20) 4x(100,0)] x 9");
	System.out.println("\t73 Tage mit 5%, 9 Tage mit 0.8 * 5% Wachstum");
	System.out.println("\t18 Tage ohne Wachstum");
	System.out.println("\t6 x 5-Tages Trockenheit(Feuchtigkeit <= 10%), wachsum / 2");
	System.out.println("\t1 x 10-Tages Trockenheit(Feuchtigkeit <= 10%), wachsum = 0");
	System.out.println("\t18 x 1-Tages Schneckenangriff(min 50% Feuchtigkeit), -1% Gewicht");
	System.out.println("\t9 x 2-Tages Schneckenangriff(min 30% Feuchtigkeit), -1% Gewicht");
	System.out.println("Expected weight = " + expected);
	System.out.println(p);
}

public static void test5(PumpkinSimulator ps) {
	
	List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
	double expected = 0.0;
	
	for (int i = 0; i < 100; i++) {
		conditions.add(new NormalCondition(100, 0));
	}
	
	Pumpkin p = ps.createSimulation(100, conditions).run();
	
	System.out.println();
	System.out.println("[TEST 5]");
	System.out.println("Wetterbedingungen (% licht, % wasser): 100x(100, 0)");
	System.out.println("\t100 tage mit 5% Wachstum");
	System.out.println("\tWeniger als 10 Tage mit 100% wasser");
	System.out.println("Expected weight = " + expected);
	System.out.println(p);
}

	public static void test6(PumpkinSimulator ps) {
	
	List<GrowthCondition> conditions = new ArrayList<GrowthCondition>();
	double expected = 0.0;
	
	for (int i = 0; i < 100; i++) {
		conditions.add(new NormalCondition(0, 100));
	}
	
	Pumpkin p = ps.createSimulation(100, conditions).run();
	
	System.out.println();
	System.out.println("[TEST 6]");
	System.out.println("Wetterbedingungen (% licht, % wasser): 100x(0, 100)");
	System.out.println("\tWeniger als 10 Tage mit 100% Licht");
	System.out.println("Expected weight = " + expected);
	System.out.println(p);
}
}
